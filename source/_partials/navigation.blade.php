<hr>
<div><a href="/">Current Talk</a></div><br>
<div><strong>Wednesday, July 25, 2018</strong>
    <br>
    <a href="/talks/adam-wathan">Adam Wathan</a>
    &bull; <a href="/talks/freek-van-der-herten">Freek Van der Herten</a>
    &bull; <a href="/talks/jason-fried">Jason Fried</a>
    &bull; <a href="/talks/samantha-geitz">Samantha Geitz</a>
    &bull; <a href="/talks/jason-mccreary">Jason McCreary</a>
    &bull; <a href="/talks/evan-you">Evan You</a>
    &bull; <a href="/talks/taylor-otwell">Taylor Otwell</a>
</div><br>
<div>
    <strong>Thursday, July 26, 2018</strong>
    <br>
    <a href="/talks/jocelyn-glei">Jocelyn Glei</a>
    &bull; <a href="/talks/matt-stauffer">Matt Stauffer</a>
    &bull; <a href="/talks/ryan-holiday">Ryan Holiday</a>
    &bull; <a href="/talks/tj-miller">TJ Miller</a>
    &bull; <a href="/talks/wes-bos">Wes Bos</a>
    &bull; <a href="/talks/colin-decarlo">Colin DeCarlo</a>
    &bull; <a href="/talks/caleb-porzio">Caleb Porzio</a>
    &bull; <a href="/talks/bob-martin">Bob Martin</a>
</div>
