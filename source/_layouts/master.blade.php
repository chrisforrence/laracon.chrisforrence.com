<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="keywords" content="laracon 2018,laracon us,live-blog">
        <meta name="robots" content="index,follow">
        <meta name="description" content="Follow a live-blog of Laracon US 2018 in Chicago!">
        <title>Laracon US 2018 Live Blog (💻 54%, ☎️ 12%)</title>
    </head>
    <body>
        <h1>Laracon US 2018 Live Blog</h1>
        @include('_partials/announcements')
        @yield('body')
        @include('_partials/navigation')
        @include('_partials/footer')
    </body>
</html>
