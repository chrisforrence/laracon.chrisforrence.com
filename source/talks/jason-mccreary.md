---
extends: _layouts/master
section: body
---

### Jason McCreary, Laravel By The Numbers

- JMac, spoke at the Louisville Laracon in 2016, created Laravel Shift
- Last talk was about YAGNI (You Ain't Gonna Need It).
- Wanted to do an iterative talk on it, but there were a few similar talks already. So what else?
- Landed on Shift for a talk topic
- Data Science Hat (suspiciously similar to a snowboard helmet), SO had R and Python suggestions
- Observations: Apparently Adam Wathan has a particular style for presenting, plaid button-down shirt ;)
- Observations: Taylor could look like someone on their way to space camp with a cap and sunglasses
- 8,470 projects with Laravel Shift
- Shifts by version, largest spike to 5.3
- Shift has 46% growth rate
- Shift has 93% retention rate (93% use it a second time)

#### Now for the data...

- Each shift, a /var/log/shift.log file gets created
- Inside a shift log, there's debug log info. Lifecycle, ancillary info
- No code/secret keys/etc. in the log file
- But looking at the logs, we can see inferred info. Like package.json, configs that couldn't be upgraded, etc.
- **20 metrics**
- These are just analytics
- Most Popular Version: **5.3**
    - Potentially because of PHP version requirements, testing framework changes
    - Not many people jump to the latest version, so **5.5** is popular
- Most Popular Packages:
    - guzzle, dbal, predis, html, flysystem, intervention/image, excel, laravel-backup, horizon, bugsnag, socialite, passport, sentry-laravel, laravel-debugbar, larvel-permission, scout, csv, laravel-cors, laravel-activitylog, pusher-php-server, cashier, laravel-dompdf
- Most Popular Dev Packages
    - laravel-debugbar, laravel-ide-helper, dusk, css-selector, dom-crawler, thanks, browser-kit-testing
- Most changed files
    - Everything in config folder (database, mail, etc.)
- A lot of people override the values directly in the config files. To make shifts easier, leverage environment variables.
- You can also do a config/core.php or config/{app's name}.php for domain-specific configuration instead of putting it in config/app.php
- Also suggest setting timezone to `env('APP_TIMEZONE', 'UTC')`
- Custom Namespacing (where App\\ gets overriden to Shift\\): **9%**
    - Does make the upgrade process a little harder
- App Structure (`find app/ -maxdepth 1 -print`):
    - (100%: Exceptions, Http, Providers, Console), (52%: Events, Listeners, Mail), Jobs, Notifications, Models, Services, Helpers, Policies, Traits, Rules, Repositories, helpers.php, Observers, Support, Facades, Transformers
    - Suggest moving helpers to bootstrap/ folder
    - 36% of projects have Models folder, usually exists when there are >11 models. Also used when subnamespaced.
- Code
    - Lot of extending BaseController, BaseModel to create separation of framework (**23%**)
    - Potentially more reasonable to do as BaseModel
    - As an alternative, go through the framework to see what else is going on. What does Laravel do when it wants to add in additional functionality?
    - Instead of extending from another base call, Laravel uses traits (e.g. `use NotifiesCreation;`)
- Customized auth: (**80%**)
    - Not just changing the redirect URL, but instead handling authentication all on their own. For example, when adding additional criteria (e.g. is active user, 2FA)
    - Laravel compartmentalizes login stuff pretty well. To augment, you can look at overriding the `authenticated()` function, or the pieces that make up authentication. Can make it easier
    - If you override it, suggest that you PHPDoc that it's been overridden
- Global Facades (global namespaces like \Mail, etc.) (**73%**)
    - Can be clearer if you explicitly use the Facade class instead of using the root namespace (e.g. `use Illuminate\Support\Facades\Mail`)
- Facades when data already injected (**57%**)
    - $request->only instead of Request::only
- Queries in views: (**24%**)
    - "This should be 0...I'm not going to say anything else about it."
- API Routes not in api.php routes file (**8%**)
- env envy (refers to configuration caching from 5.3/5.4) (**42%** can't do it since you're using `env()` instead of `config()`)
- _Actually_ CRUDdy Controllers (**77%** don't follow)
    - Listen to Adam Wathan's talk from Laracon 2017
- Empty Controllers (no actions) (**26%**)
    - May be from new developers using controllers as an object, using it outside of handling requests
    - May also be because of improper visibility

#### Features

- Controller Validation (**89%** validate in the controller method)
    - Option: use a Request object with contains the validation rules, then `store(StorePost $request)`
- Blade Directives (e.g. @auth/@endauth, @guest/@endguest, )
    - Instead of csrf_field(), use @method('PUT') and @csrf
- Custom Events (**19%**)
    - Can include event listeners
- Eloquent Relationships (**87%**)
    - belongsTo used 87%, hasMany, belongsToMany, hasOne all above 50%. Then morphTo, morphMany, hasManyThrough, morphToMany, morphedByMany, using)

#### Stats

`composer require --dev wnx/laravel-stats`

- Includes additional statistics on classes, including methods/class and lines-of-code/method
- Tests (**27%** use tests)

#### Laravel Analyzer

- Free until Friday, July 25

#### Coupons

- https://laravelshift.com/promotions/laracago18
- https://basecodefieldguide.com/promotions/laracago18

#### Q&A

- When application fires events, how do you track events getting fired in mental overhead
    - Most people seem to be doing it in service providers
    - Multiple places, so it can be disjointed.
- How many Laravel 4 applications still out there?
    - Not really sure, still a few Shifts per week coming from 4.2
- Shift all the way up in one go?
    - Not recommended, recommend step-by-step to make it easier
- You *could* try just doing `composer update`, but that could get tricky between major versions
- Shifts are completely automated
- Follow on Twitter: @gonedark
