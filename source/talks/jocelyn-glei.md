---
extends: _layouts/master
section: body
---

### Jocelyn Glei, Hurry Slowly

www.hurryslowly.co

I'm here to convince you that you can level up by slowing down.

- 20 years of working
- Podcast is Hurry Slowly, a podcast about pacing yourself. Helps you be more resilient, etc.
- Poll
    - Raise hand if you work really hard (about 20%)
    - Raise hand if you think you can do it for next 10-20 years (half of that)
- The way we're working is not sustainable
- Every year, there's a survey "General Social Survey". When asked how exhausted respondants were, 50% responded they were constantly exhausted from work
    - Up from 18% in comparison to two decades ago
- There's a burnout epidemic
- Our default state is overwhelmed, over-stimulated, flight-or-flight
    - Constantly on high alert, adrenaline and cortisol pumping
- 2013, worked at startup named 99U. Going well, but went crazy, 3X workload, putt ogether a huge amount of work, led to busyness breakdown
- Can start to age you after a while.
- The things that put you into fight-or-flight mode changed from lion to mail/Facebook/Twitter/Slack/calendar/messages
- 29,000 hours over your career that you could potentially spend on EMAIL alone. 3.3 years
- Average person sends 200 messages/day, 2.5hr/day on email, 30% of workweek on email
- Useful? Managers spend 50% of time reading and responding to emails that should not have been sent to them and emails that they did not need to answer. Maybe not useful
- 37,000 hours in meetings (4.2 years)
- Senior executives spend roughly 2 days/week in meetings, 15% of organization time
- Senior executives rated +50% as ineffective or very ineffective
- 42,000 hours spent "multi-tasking" (4.8 years)
- "Even the brief mental blocks created by shifting between takss can cost as much as 40% of productive time" - David Myers
- Takes about 25 minutes to get back up to speed after you interrupt yourself to do something else (even just "quickly" checking email)
- All to make a point: we've gotten very good at making time for busy work, very bad at making time for best work
- It's not that we're not doing the work that really matters, it's that we're adding on to the time.
- Answer to break out of cycle comes down to one word: **Progress*
- Progress keeps us motivated, but also achilles heel
- A few years ago, Teresa Amabile(sp?) wanted to find what we find motivating. From research, found that the single most important thing is **making progress in meaningful work**
- Not just launching new version of app, got rid of huge amount of debt, but really matters is daily, incremental progress. Seeing yourself take baby steps towards completing a task. Small wins.
- We're wired to seek completion. (GIF of "You complete me")
- What that means is that we're predisposed to focus on quick, easy-to-finish tasks.
- Completionize: for example, Inbox Zero ("Wohoo! You've read all the messages in your inbox."). Little hits of completion, sense of progress.
- "Feels" like progress: checking email, managing Slack notifications, social media feeds. Addiction to getting rid of the red notification buttons
- The apps and tools we use give us a **false sense of progress**, busy work. It's just empty calories, doesn't contribute to **meaningful work**
- How to make time for meaningful work
    - Holding space, minimizing distractions, saying No

#### Holding Space

- Comes from child psychology. When raising a child, concept of creating a home environment. Can also apply to adults.
- A space where you are free from the stress of interruptions, and have the time to do meaningful work.
- Step 1: Go on the offense with your calendar.
    - If you find you rarely add/decline events, it's usually a signal that you're being too passive with how you're spending your time. - Julie Zhuo
    - One of first interviews was with Jason Fried. "If you don't have your own time, you don't have control over your day.". When you don't have control over day, you end up working longer than you should
    - Calendar is first place to start taking control over day
    - Book daily 90-120 minute focus blocks into your calendar _weeks in advance_
    - 90-120 minutes is about the max amount of time people usually can keep a deep focus
    - Most people, even high performers, can only do those kinds of sprints three times a day
- Step 2: Establish "core hours" with your team
    - Set a limited number of hours that everyone has to adhere to, when people can book meetings
    - e.g. core hours from 11am - 4pm
        - All meetings happen between 11-4
        - Slack availability between 11-4
        - Early birds (or parents) could work 8-4
        - Night owls (or 20somethings) could work 11-7
    - Stick to core hours, that means you could book your focus times without the fear of interruption
    - Could be daunting for full organization, but could start with small team

#### Minimizing Distractions

- \*cough\* email slack \*cough\*
- Step 1: Don't use key cognitive time to process email
    - For most of us, body is primed for peak cognitive performance from about 9-12 (true for maybe 60% of people in room)
    - Try to start the day with meaningful work.
    - Try and build in a focus block during those peak hours
- Step 2: Switch to "batch processing"
    - Set a few windows every day to power through email, notifications. When not in those winows, ignore email
    - Batch processing makes you less stressed, more productive, happier
    - Sounds like a good argument for doing that :)
    - Batching makes it easier to prioritize what's important
    - When processing emails one by one, harder to see what's really important. Eaiser to handle multiple at once so you can prioritize
- Step 3: Use the 80/20 rule
    - 80% of your results come from 20% of your efforts.
    - What if only 1 out of every five emails mattered?
- Step 4: Set expectations and stick to them
    - What's the most important thing when training puppies? Consistency
    - If you respond to an email within 5 minutes, I expect to get a response from you within five minutes.
- Step 5: Align with core hours availability
    - 8-11: flex hours/ DND
    - 11-4: core/ availabile

#### Saying No

- Thinking about ideas between having two selves: physical self bounded by time and space, 24 hours in a day, but we also have digital self. Composed of multiple inboxes (email, social media)
- Digital selves have sort of an infinite capacity. Email never stops accepting emails/requests
- Digital self is happy to let requests stack up infinitely
- In this landscape, we have to be very good at saying No
- You have to say NO to some opportunities so that you can say YES to your priorities.
- Step 1: Make a "stop-doing list"
    - It's an opposite of a todo list! Idea from Jim Holland
    - Not a daily activity, but something to sit down and do every 3/6 months
    - Make a short list of habits/activities that are draining your time and attention
    - Doesn't have to be long, could be just 4-5 things
    - Ideas:
        - Don't treat stranger emails as urgent
        - Don't read the news during work hours
        - Don't schedule meetings in the morning
        - Don't eat at your desk
    - What are the behaviors that are draining away focus? Write those things down so we're all on the hook to stop doing those behaviors
    - Makes it easier to call people out when doing those Stop-Doing behaviors
- Step 2: Shift from "I can't do that" to "I don't do that"
    - "I can't check email on Saturdays" vs "I don't check emails on Saturdays"
    - "I can't skip my gym workout" vs "I don't skip my gym workout"
    - When people shift their language, they're more effective in sticking to that resolution
    - Shift in language especially effective for email
    - When you tell someone you can't do something, it opens the door to being able to do something under different circumstances
    - When you say you don't do something, it's more firm
    - Making that shift in email makes it more powerful
- Step 3: Use the "qualified yes"
    - When you can't say no
    - Trend towards saying "Yes, and...". Fine for brainstorming, but not great for examining/managing workload
    - Instead, try using "Yes, but..."
    - "Yes, I can prioritize that, but that will pull resources away from Project X. Which one is more important to you?"
    - Make _them_ pick which to prioritize
    - To articulate your workload to someone is important.
    - Eaiser when it was 1 project, 1 manager. Now it can be multiple projects with multiple managers.
    - Gets you in a situation where nobody knows everything you're working on except for you; you know what you have on your plate
    - Can be harder when there's a lot of work, but important
    - "The creation of rules is more creative than the destruction of them." - Andrea Zittel
    - Sort of a strange thing for an artist to say, but if you think about artists, entrepeneurs. Not that they don't play by _any_ rules, it's they play by _their_ rules.
    - Rules are power. They determine the objective, what actions you can/cannot take, define decisions
    - We live in a world where, if you don't set the rules, technology will set them for you
    - Email taught us to treat other people's demands as urgent. Shared calendars taught us our time wasn't our own.
    - Social media taught us to value documenting the moment over being in the moment
    - Tools make excellent servants, but very poor masters
    - Set boundaries

@jkglei
iTunes: Hurry Slowly
