---
extends: _layouts/master
section: body
---

### Wes Bos, Learn CSS Grid

- From Canada
- JS guy giving a CSS talk at a PHP conference...wat
- "Who invited this guy?" "Who's inviting all these Canadians?" "Sorry about that"
- Maybe "Top 10 Code Igniter Tips", "Editing Files Straight From The Server" "Laravel.js"
- Nah, let's chat about
- **Slides will be available after the talk**
- Wes has a bunch of courses, including one on CSS Grid
- You'll leave with a good idea of what it is, when to use it and all its pieces

#### CSS Grid

- It's a new addition to the language (not a framework or layout)
- Core Ideas: grids and items
- Step 1: Define a grid, then slice it up into rows and columns
- Step 2: Put items into grid
- Step 3: The code looks like...

```
.grid1 {
    display: grid;
    grid-template-columns: 300px 300px 100px;
    grid-template-rows: 200px 300px 100px;
    grid-gap: 20px; /* Spacing, gutter */
}
```

- Tracks are columns and rows
- Slice the grid into columns and into grows.
- By default, items span one spot.
- Grid items can be anything (as long as it's a direct descendent of the grid)
- If you have content larger than the spot, it'll overflow
- What happens if you have more items than slots?
- Explicit grid is what gets defined, implicit grid is when there's left over.
- Height of the row in the implicit grid is defined by the tallest item
- You can also set the height of implicit rows (`grid-auto-rows`)
- You can even have a 100% implicit grid
- You can change the axis from LTR (left to right) to TTB (top to bottom)
- Once explicit spots are used up, additional rows get created...but you could change that to add additonal COLUMNS instead (`grid-auto-flow: column;`)
- How to size tracks (remember, rows and columns)
- We can specify the dimensions using any current measurement unit (e.g. rem, ch, px, in, vh)
- Percentages too (fine when use in combination with auto)
    - `grid-template-columns: 80% auto;`
- Not for adding up to 100%...
    - 80% + 20% + 20px... (20px, remember that whole grid-gap?)
- Solution is not to use perctantage, but fractional units.
    - `grid-template-columns: 8fr 2fr`
- Browser will first dediate space towards things that need a specific amount of room (existing content, fixed sized tracks, grid-gap). Remaining space divided up in proportion (like flex-grow)
    - Like a budget; you can only play with your money (space) after paying the gap tax, fixed size tax...
- Another part is the `repeat()` function. calc(), minmax(), color() coming soon...
    - `grid-template-columns: repeat(5, 1fr);`
    - Can be used multiple times
        - `grid-template-columns: repeat(3, 1fr) repeat(2, 3fr);`
        - `grid-template-columns: 200px repeat(3, 1fr) 100px;`
- How do you size items?
    - If it has a fixed width, that'll size the track, but that'll take everyone else along that track for the ride
    - In most cases, the best way to size grid items is to have it span multiple grid spots
        - ```
        .item:nth-child(3) {
            grid-columns: span 3
        }```
        - If it spans 4, it'll go onto the next track, leaving empty space behind
        - You can use `grid-auto-flow: dense` to fix this
- Line Numbers
    - Tracks are numbered by gutters, not the tracks
        - In a three-column layout, there are four line numbers
    - -1 spans to the end of the explicit grid (`grid-row: 1 / -1;`, like for spanning the entire top of a grid)
    - Can specify where an item ends (`grid-column-end: -1; grid-column-start: span 4)`)
- auto-fit vs auto-fill
    - Grid with flexible width, 20px of gap. How many 100px tracks can we fit?
        - A: IDK, depends
    - `grid-template-columns: repeat(auto-fill, 100px)`, it'll put as many columns as will fit
    - Auto-fit won't create tracks out of empty space, but auto-fill _will_.
    - Auto-fill will dice up those spots (Wes used an example of a cucumber)
- Why? auto-fit and auto-fill are really meant to be used with minmax()
    - `grid-template-columns: repeat(auto-fill, minmax(350px, 1fr))`
    - Won't leave space at the right of the grid
    - Cool; it's responsive without relying on media queries!
    - Container-aware, doesn't rely on the viewpoint
    - Really, REALLY handy
- Grid Template Areas
    - Another way to define where items go is to name them
        - `grid-template-areas: 'head head head head'
                                'side side main main'
                                'ads ads main main'
                                'foot foot foot foot';`
    - To put those things in the area, set `grid-area: head;`
- Named Lines (when you name your areas, you get named line for free!)
    - `grid-column: content-start / content-end; grid-row: content-start / content-end;`
    - You can also explicitly name the lines
        - `grid-template-columns: [sidebar-start site-left]
                                  1fr
                                  [sidebar-end content-start]
                                  ...`
- Alignment and centering, CSS Grid great for just aligning elements
    - Six alignment properties: justify-\*, align-\*)
- justify-content for aligning the tracks themselves
- justify-items to align the elements inside the track
    - By default, stretch
    - Can also center, start, end, can justify-self to override justify-items on a single element)
- align-content
- align-items
    - `baseline` will make sure all the text is lined up regardless of the div size
- That's a high-level overview on the pieces of CSS grid
- Free course at cssgrid.io
- CSS Grid vs Flexbox...not _really_ competitors
    - Flexbox is one axis, CSS Grid is two
    - Here is something only Flexbox can do...here's something only CSS Grid can do

#### Real Website Use Cases

- Component examples (also good for finicky parts)
- Firefox's dev tools good, Chrome's is alright
- Can re-order elements
- Perfect for overlapping items

When dealing with an unknown number of items...

- dense block-fitting grid (Pinterest)
- Also page bleed
- Well worth learning
- Get at it, he's got stickers
