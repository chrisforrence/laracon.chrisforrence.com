---
extends: _layouts/master
section: body
---

### Evan You, Vue CLI Overview

Introducing Vue CLI 3.0

- A decent number of attendees have used the CLI 3.0-RC!
- What is Vue CLI?
    - Laravel has its own CLI between the global laravel command and artisan
    - In the very early days, it didn't make as much sense to have CLI for front-end
    - Nowadays, you pretty much have to use a build stack
    - **Standard tooling for building Vue applications**
    - Makes it easier to integrate with CSS preprocessors, typescripting, PWA integrations, linting, testing
- Vue CLI vs Laravel Mix
    - Laravel Mix is lightweight, suits a widget/enhancement style usage of Vue
    - Vue CLI is focused more on complex frontends, ships with more front-end tooling options (like testing, linting, PWA, etc.)
- Previously, you could see `vue init webpack hello-world`
    - More used as a scaffolding tool
    - Thin layer of repo-based scaffolding
    - Tooling configurations directly contained in repo
    - Advantages: simple CLI implementation, open to customization, easy to create community-forked templates
    - Disadvantages: Templates not upgradable, webpack configuration can be intimidating, difficult, complex over time, forked templates need to constantly sync with upstream
        - Putting everything into one template can make the file a mess, hard to maintain
- Challenges
    - Ease of use vs features & options
    - Customizability vs upgradability
    - Extendibility vs conventions
- Goals for the new versions
    - Zero config by default
    - Add additional features shipped as built-in plugins
    - Everything should be configurable without being ejection
    - Everything not built-in should also be implementable as a plugin
- So, with zero config...`vue create -d hello` (old vue-cli is `vue-cli`, new vue-cli is `vue`)

```
$ vue create -d hello
? Please pick a preset: (Use arrow keys)
> default (babel, eslint)
  Manually select features
```

or...

```
$ vue create --default -d hello
```

- Includes /src, babel.config.js
- package.json includes three scripts: serve, build and lint
- postcss, browserslist for babel and postcss
    - browserslist
- Can also do manual

```
$ vue create -d manual
? Please pick a preset: (Use arrow keys)
  default (babel, eslint)
> Manually select features
```

- (there are additional questions, prompts whether to use class-style component syntax, Babel, choosing a CSS preprocessor, linter, additional lint features, unit testing solution, where to put config files)
- Whoops, live-👨‍💻 had a slight issue
- Scaffolded Jest \*.spec.js, can lint even on typescript
- Or...instant prototype: `echo '<template><div>hello</div></template>' > App.vue && vue serve`
- Once happy with the prototype, you can serve it up
- Comes with a UI webpage!...on a CLI (got chuckles from the audience)
- Creating via the UI threw an issue...whoops. Good thing we already have a few!
- A good number of plugins are available, official ones are prepended with `@vue/`
- Installing CLI...conference wifi...linking dependencies...took only around 15 seconds!
- It'll also show file diffs, ask if you want to commit the change (the addition to package.json)
- Was going to show off analyzer (which files are using up the most space in your app)

- Configurable if needed
    - vue.config.js, or webpack (webpack-merge, webpack-chain), babel/postcss/eslint/typescript can use dedicated config files
    - Because it doesn't make sense to force you to use one config method
- You can do in-project config in the vue.config.js
- You can bring in another project's configuration with chainWebpack
- `vue inspect`
- Can be useful if you want to use vue-cli's dev server but want to serve the back-end separately
- Chainable webpack configuration, can tap a named plugin
- Extensible via plugins (e.g. `vue add @vue/pwa`)
- Custom plugin API
    - Can tap and extend core webpack config
    - Register custom commands (like vue-cli-service test)
    - Inject prompts and files generator)
- Build targets
    - App mode by default, but you may want to share it as library or web component
- `yarn build --target lib` assumes that you're building a component library, so different dist contents
- The demo.html file that also gets generated includes your built component

- `yarn build --target wc --name wc-demo` (builds as web component)

```
    <script src="https://path/to/vue.js"></script>
    <script src="./wc-demo.js"></script>
    <wc-demo></wc-demo>
```
- Shippable with the exact same codebase, but packaged/shipped differently!
- Can ship a collection of native web components
    - `vue-cli-service build --target wc-async 'src/components/*.vue`
    - Important, can do code-splitting. Because when you have a large number of components, don't want to load all 30 components up-front.
- Modern mode allows you to ship native, lean and fast ES2015+ today with `<script type="module">`
- We can also ship native/ES2015 to modern AND ONLY modern browsers, then ship the babel'd version to legacy browsers!
- Babel can give you a target that only needs polyfills/such for modern browsers (which is way less than what'd needed to bring, say, IE11 into compliance)
- `yarn build --modern`
    - Builds one version for legacy, one version for modern.
- To load modern, `<script type="module" src="/path/to/script">`
- To load legacy, `<script nomodule src="/path/to/script">`
- Integration with laravel: https://github.com/yyx990803/laravel-vue-cli-3
- Created frontend folder. devServer proxy to proxy API requests to valet during development (frontend/vue.config.js)
    - outputDir (static files)
    - chainWebpack Laravel doesn't know to generate these things, so change the output path to resources/views/index.blade.php.
- vue serve ends up serving, can access the Laravel API for a Spa App
