@extends('_layouts/master')

@section('body')
<h3>Freek Van der Herten <br /><small>Tour of Laravel MediaLibrary v7</small></h3>
<a href="https://t.co/YQQMWw7gEd">Slides</a>
<ul>
    <li>Handling media in a Laravel app</li>
    <li>Here's the problem: For user uploaded content, where to store it? Retrieve it? Optimize images?</li>
    <li>Medialibrary. It can associate files with eloquent models (e.g. Post), group media into collections, convert images (e.g. scaled down images), multiple file systems</li>
    <li>Can also optimize converted images (strip out metadata), create custom images (e.g. watermarks), customize the directory structure (by default, maybe use IDs, but if you want to hide how many images you have, maybe used hashed ID), and add other custom attributes</li>
    <li>As a workflow, add files, store information in model in database, generate derived formats...</li>
    <li>Documentation at docs.spatie.be/laravel-medialibrary</li>
    <li>It's getting popular</li>
    <li><h4>DEMO</h4></li>
    <li>(working on getting the demo set up)</li>
    <li>Route::get('add-media-to-library', function () { Article::create()->addMedia(storage_path('demo/museum.jpg))->toMediaCollection()'))}</li>
    <li>Article model uses HasMediaTrait, implements HasMedia</li>
    <li>`Article::last()->getMedia()` and you get stuff back!</li>
    <li>Article::last()->getFirstMedia()->getUrl(), getPath()...</li>
    <li>Instead of `addMedia`, you're more likely to use `addMediaFromRequest`</li>
    <li>Using collections: addMedia(storage_path('path/to/image'))->toMediaCollection('images')</li>
    <li>Using collections: addMedia(storage_path('path/to/zip'))->toMediaCollection('downloads')</li>
    <li>Live-coding, resolving 500 error</li>
    <li>By default, getMedia gets uncollected media. Need to call getMedia('images') or getMedia('downloads')</li>
    <li>getFirstMedia('downloads')->getUrl() ✔️</li>
    <li>Delete models, `Article::all()->each->delete()`</li>
    <li>Using media conversions:</li>
    <li>In model, public function registerMediaConversions(Media $media = null)</li>
    <li>$this->addMediaConversion('thumb')->width(400)->height(400);</li>
    <li>$this->addMediaConversion('pixelated')->pixelate(5);</li>
    <li>Takes a little bit longer since it needs to generate the files, but works like a champ</li>
    <li>Article::last()_.getFirstMedia('images')->getUrl($nameOfConversion, e.g. 'thumb')</li>
    <li>If you have a queue configured, use that so that you can generate that off-hand. Or you can include nonQueued() as part of the media conversion</li>
    <li>Optimized images by default, but you an add nonOptimized() as part of the media conversion. Use cases for artwork where you <i>want</i> the metadata</li>
    <li>PDFs can get converted too! LIke a Laravel Nova PDF...one page, just a landing page. And hey, you can generate a thumbnail of the PDF!</li>
    <li><h4>Customizing the path</h4></li>
    <li>In the PathGenerator medialibrary pathgenerator, implement BasePathGenerator, override getPath (return md5($media->id . time())) as example</li>
    <li>in config/medialibrary.php, `'path_generator' => App\Services\MediaLibrary\PathGenerator::class`</li>
    <li><h4>Using S3</h4></li>
    <li>The toMediaCollection call takes a second parameter: the name of the file system (e.g. s3)</li>
    <li>Lousy Wi-Fi at the venue, so we're saying the s3 filesystem is actually local (but at a different path than the regular local)</li>
    <li>With larger files, advise using something like S3. We can register a media collection in the Article model</li>
    <li>$this->addMediaCollection('big-files')->useDisk('s3');</li>
    <li>singleFile() can be added on if you want to overwrite old files (e.g. a user avatar, background, etc.)</li>
    <li>acceptsFile(function(File $file) { return $file->size < 1024 * 10; }); to be able to reject files too large</li>
    <li><h4>Downloading a file</h4></li>
    <li>$media = Article::create()->addMedia(...)->toMediaCollection(); return $media;</li>
    <li>And hey, that downloads the file!</li>
    <li>Also works from other filesystems (e.g. toMediaCollection with s3)</li>
    <li>If you want to download multiple files, in a project, you may copy all those files locally, zip them and transfer the zip. Medialibrary handles it a little better</li>
    <li>Create articles with media, then `return MediaStream::create('files.zip')->addMedia(Media::all());`</li>
    <li>It won't create the zip file on the server; it streams the files so that it creates the zip only for the client (got applause)</li>
    <li>Generating responsive images, you can do addMedia(...)->withResponsiveImages()->toMediaCollection()</li>
    <li>Generates a huge number of images, depends on the size of the original </li>
    <li>Now to show those responsive files...</li>
    <li>compact('media'), then your view just has `{\{ $media }}`</li>
    <li>It uses a srcset with all of the available images, including an auto-blurred SVG that displays while loading the image. Uses w to check how wide the screen needs to be to use that images</li>
    <li>It'll cancel image downloads as it goes up the srcset if there's a larger image that matches</li>
    <li><h4>End of demo, let's wrap up</h4></li>
    <li>Resources:<ul>
        <li>https://github.com/spatie/medialibrary-demo-laracon-us</li>
        <li>https://docs.spatie.be/laravel-medialibrary/v7</li>
        <li>There's also a YouTube link</li>
        <li>https://github.com/spatie/laravel-medialibrary</li>
    </ul></li>
    <li>Coming soon: vue components, uploads, administering a media collection, targeting Q3/Q4 2018</li>
</ul>
@endsection
