@extends('_layouts/master')

@section('body')
<h3>Adam Wathan <br /><small>Resisting Complexity</small></h3>
<ul>
    <li>Fundamental misunderstanding about OOP for a long time that made code unnecessarily complex, less fun to work with</li>
    <li>Example: here's a plant that needs water. Save the dying plant! If I were doing this in code, "I need something to water this plant. What has the ability to water plants?" Model domain in hand. Needs a gardener!</li>
    <li>$gardener = new Gardener; $gardener->water($plant);</li>
    <li>This is how I thought about design for a long time; create an object to deal with a problem that comes up!</li>
    <li>Looking at example again, let's break it up.</li>
    <li>A GARDENER CAN WATER A PLANT: A {object} CAN {method} (a {parameter})</li>
    <li>names.reverse(): An Array can Reverse</li>
    <li>names.split(', '): A name can split a delimier...um...wat.</li>
    <li>What we're really saying is a string can BE split USING a delimiter</li>
    <li>A {object} CAN {method} (a {parameter}), methods are being treated as affordances</li>
    <li>Affordances tell you how an object can be used. e.g., a handle is an affordance; pull it to open the drawer</li>
    <li>A ** can be ** (using a **)</li>
    <li>$user->save(); A user can be saved (got laughs from the crowd)</li>
    <li>Jumping back to the gardener up above, translating to the A-can-be format...a gardener can be watered using a plant! wat.gif</li>
    <li>It's not that a gardener can be watered, it's that a plant can be watered</li>
    <li>So instead of $gardener->water($plant), $plant->water();</li>
    <li><h4>Eliminating Agent Nouns</h4></li>
    <li>Agent noun: dnoting someone/something performing the action of a verb (e.g. worker, accelerator)</li>
    <li>Let's take a look at how to eliminate agent nouns</li>
    <li>Code is being shown: AnnouncementsController, constructor takes in an AnnouncementBroadcaster. Store method creates an announcement, calls, $this->announcementBroadcaster->broadcast($announcement);</li>
    <li>But let's try this instead: $announcement->broadcast();</li>
    <li>Advantages: promoting encapsulation</li>
    <li>The broadcast method updates its broadcast_at parameter, then dispatches a BroadcastAnnouncement job</li>
    <li>Let's try Queue::dispatch(new BroadcastAnnouncement($this);</li>
    <li><h4>Conquering your fear of facades</h4></li>
    <li>Fear of Facades is mostly because it's depending on the framework, so you can't just take it somewhere else.</li>
    <li>Changing `$date = new DateTime('now', 'America/Chicago'); $date->format('Y-m-d H:i'); $date->`</li>
    <li>Uses the computer's timezone mapping, but ergonomics matter. Ergnomics can make the trade-off between flexibility palpatible</li>
    <li>Example: Back in the announcement->broadcast(), you can instead pass in a dispatcher into the broadcast method, then $announcement->broadcast($dispatcher)</li>
    <li>Refactor `Queue::dispatch(new BroadcastAnnouncement($this);` to `BroadcastAnnouncement::dispatch($this);`</li>
    <li>NEW EXAMPLE: ProductPurchaseController</li>
    <li>First refactor: $this->purchaseFullfillmentService->fulfillPurchase($purchase);</li>
    <li>to...$purchase->fulfill()</li>
    <li>fulfillPurchase($purchase) from service gets copied into purchase model</li>
    <li>public funtion fulfill() lets you use $this instead of $purchase. Then $this->mailer->... can be Mail::to</li>
    <li>You can also <h4>push side work to event listeners</h4></li>
    <li>PurchaseFulfilled::dispatch($this); in model</li>
    <li>A better way to name a listener is to describe the job (e.g. SendPurchaseFulfillMail)</li>
    <li>The new listener handler handler just becomes $this-mailer->to($event->purchase->send_to)...</li>
    <li><h4>Breaking up God Objects</h4></li>
    <li>Objects that tend to attract many methods, mostly because of tendencies to put methods on classes that can do stuff. Like users. Users can do lots of stuff!</li>
    <li>NEW EXAMPLE: LicenseRedemptionsController</li>
    <li>Auth::user()->redeemLicense($license); in store method</li>
    <li>We want to redeem the license, so `$license->redeem(Auth::user())`</li>
    <li>Now the license model has a purpose aside from just being an eloquent model</li>
    <li>In redeem method, update user_id => $user->getKey(), LicenseRedeemed::dispatch($this);`</li>
    <li><h4>Uncovering new concepts</h4></li>
    <li>By thinking of things as affordances, you may create different objects</li>
    <li>Back to the product purchase controller, there's validating the product, charging with the payment gateway, creating the purchase entry, then fulfilling it.</li>
    <li>It's almost like we're trying to complete a purchase.</li>
    <li>First though: $puchase->complete(), but it's not really a concept yet; it's before the purchase. Instead, let's think about a checkout.</li>
    <li>Using the checkout concept, `$checkout = new Checkout($product, request('email')`</li>
    <li>Now we can complete the checkout (`$checkout->complete()`). What else needs to be included when completing a purchase? Maybe the payment gateway, payment token</li>
    <li>$purchase = $checkout->complete($this->paymentGateway, request('token');</li>
    <li>Let's new up a checkout class! Methods with `complete($paymentGateway, $paymentToken)`</li>
    <li>Checkout class has private class varaibles of product and email</li>
    <li>Now the controller is just creating a checkout object, calling $checkout->complete and storing that as $purchase, then returning $purchase</li>
    <li><h4>Maybe you just need a function</h4></li>
    <li>In the purchase class, we create licenses, each with a random code</li>
    <li>In `PurchaseTest`, in testing, you need to be able to stub things out so you can make assertions about it. When doing a random string, though, you can't guarantee a random 32-character string. For some reason.</li>
    <li>So, how can we test this? app(LicenseCodeGenerator::class)->generateLicenseCode()) of course! /s</li>
    <li>No, we just want a function to generate a license code. `app('generateLicenseCode')->__invoke()`.</li>
    <li>Adam got a green test! (he got it written 15 minutes before the talk)</li>
    <li>In the test, $this->app->instance(GenerateLicenseCode::class,  function() { return 'abc123'; })</li>
    <li>Create a GenerateLicenseCode class, invoker generates a random string</li>
    <li>Another way to make the code ergonomic: `'code' => LicenseCode::generate()`</li>
    <li>LicenseCode has a generate function that resolves GenerateLicenseCode</li>
    <li>That way, we can override the functionality for testing</li>
    <li>Try not to reach for creating classes that can do that thing. Hope you thought about OOP a little differently</li>
    <li><h4>Thanks! @adamwathan https://adamwathan.me</h4></li>
</ul>
@endsection
