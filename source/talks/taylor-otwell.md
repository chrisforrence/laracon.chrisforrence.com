---
extends: _layouts/master
section: body
---

### Taylor Otwell, Keynote

[Official video](https://www.youtube.com/watch?v=pLcM3mpZSV0), [Live-Stream](https://www.youtube.com/watch?v=UdCZVeqYwRw) courtesy of [Freek Van der Herten](https://twitter.com/freekmurze)

- 6th Laracon US, 12th Laracon overall
    - 90 people at first one, Laravel 4.0 was announced to come out in May
- 850 people at Laracon US 2018

#### Updates

- Slated to release the next version of Laravel in August
- Releases have gotten less radical over time
- What about Laravel 5.7?
    - Breaking changes are minimal

#### New Features

- New interface for email verification
    - When someone signs up, they get an email for confirming their email
- Policies have optional typehint
    - `public function methodname(?User $user)`
- php artisan dump server can dd stuff to your CLI, not the browser

#### LARAVEL NOVA

- Laravel Nova is a beautiful admin panel system
- Installed as a Composer package, can be installed in existing Laravel applications
- Built as a SPA using Tailwind CSS
- User.php has a fields method that returns an array of components
- It doesn't store configuration, purely code-driven
- Can drill down to a view page, includes relations information (e.g. posts, roles)
- Can group fields into panels
- Can create new resources within Nova
- Can edit/update users
- Events from Nova still get logged
- When defining Nova fields, you can add validation rules
- It'll detect if it's been updated in the time between when the data was first requested and when it should save
- Soft deletes? You can filter to include trashed/only trashed, and you can restore the resource from there. Can also force delete
- Looking into other fields...
    - Dates automatically display in local time (like CDT), but are stored in UTC
    - Markdown (instead of Textarea for example)
    - Trix (WYSIWYG) (instead of Textarea)
    - Parse JSON field `Code::make('Options')->json()->rules('json')`
    - Can also be as PHP
- Nova won't show text areas by default on the index
- Photo resource...you can extract a field out into another function (e.g. `photoField`)
    - If you don't edit the photo, it won't delete the old photo (unless you explicitly remove it)
    - Works with invokable classes (`->delete(new DeletePhoto)`)
    - Prunable...if we delete the resource, the underlying file will also get deleted. Cleans it up!
- BelongsTo...
    - "Let's go ahead and add 5000 users"
    - Add `->searchable()` to BelongsTo
- Nova can also take advantage of Scout!
- Nova Search (search for 1, it'll find all the things with 1) (and it can even include auxiliary data, like a relation to that)
- Can also make address fields (as an example)
    - Text -> Place, 30QPS
- Many-to-many relationships? ✅
    - Updating it updates the pivot table, deleting it detaches the resources (User and Role, for example, still exist)
- Polymorphic relationships
    - Comments can belong to Videos or Posts
    - Pick Commentable Type, then Which One
    - Can also work with trashed resources!
- Can "fill in" fields straight from an API
- Can add filters (e.g. Only Active Users, Only Inactive Users)
    - Filter class has apply and options method, like Eloquent for UI elements
- With a filter set, it'll carry over, apply
- But wait, there's also a CLI! `php artisan nova:*`

#### Lenses

- Used to aggregate data or build reports
- So fields, query builders
- Attach a lense object by adding the object to an array returned in lenses
- Lenses can have filters attached, query just needs `->withFilters`

#### Actions

- Instead of just CRUD actions, you can take specialized actions (e.g. deactivate a user, refund user)
- When working on Nova, thought about "What actions would I do?"
- Easy to scope actions
- Can extend DestructiveAction to change the confirmation button from primary to red
- Actions get logged (including by whom)

#### Long-Running

- Can implement ShouldQueue
- Actions when long-running, show up in actions log as "Running", page updates when done

#### Actions on ManyToMany

- Every action has a fields function
- usersField and rolesField have action for updating notes
- In example, can update notes on multiple users at a time
- tl;dr can be context-sensitive

#### Metrics

- Three types of metrics:
- "How many users created in this quarter compared to the previous quarter?"
- 🚨👨‍💻
- Metrics have `calculate` method and `ranges` method
- User resource has `cards` resource
    - 70 total users over 30 days (8.33% Increase)
    - Can also show "No Prior Data"...if...no prior data
- `php artisan nova:value TotalUsers` creates a single value
- `php artisan nova:trend NewUsers` creates a line chart
- `php artsian nova:trend LicenseRevenue`, you can calulate with a sumBy
- `withLatestValue()` to also show the latest value in the card
- Dashboard cards will show when they appear (loading by default) and are cached

#### Authorization

- Nova can use Policy to handle Nova authorization (if users can edit/view/delete certain resources)
- addModelName as naming convention, attachModelName
- Actions can take in canSee for whether or not a user can see something
- Front-end and back-end validation

#### What if you want to add your own stuff?

- You can add custom tools, custom resource tools, custom cards, custom fields
- `php artisan nova:tool Thing` creates nova-components/Thing.vue
- Anything you can do in a Vue component, you can do as a Nova component. Can be private, public, whatever.
- Custom resource tools (like for interacting with the Stripe API to get charges)
    - Easy to pass stuff down into your Vue component
    - Still a Vue component, so I can do whatever I need
- Custom Cards
    - Appears on dashboard (help cards are default custom cards)
- Custom fields
    - Three Vue templates: one for the Detail Screen, for the Index Screen and for the Form Screen
    - Example shown was for Favorite Color

#### That's the overview of Laravel Nova

- He launched the site JUST NOW
- Two prices: $99/license for single developer, $199 for team (similar to spark)
