---
extends: _layouts/master
section: body
---

### Caleb Porzio, Embrace The Backend

- I'm Caleb. I work for Tighten. Podcast of twenty percent time. 20% of audience has listened it
- Used to do Blade components, then Vue, then Vue components with inline-template. Then everything changed. Single-file Vue components
- Before .vue, "Whoops, looks like something went wrong." Now...console.error, endless fa-spinners, "Whoops, something went wrong" Sweetalerts...and worst of all...passing tests.
- Belly of the beast
    - `<twitter-timeline></twitter-timeline>`
    - Let's dive in.
        - Tweet input component, loading spinner, tweets component if not loading
        - Data is the reactive data
            - Idiom that he sometimes uses is loading and busy
        - On created, get tweets, handle resolved/rejected promise
        - when posting a tweet, we handle it again, but there's a lot of state management.
        - Server on left, UI on right, Vue component in the middle with props, data, created, computed, methods.
        - Data flowing all over.
- It had felt so right...but now I was afraid of my front-end
- How do we do better? We can JavaScript better. We can use Form objects. Axios Interceptors. Vuex. Jest. Vue Router...but that's not what this talk is about.
- Instead...Laravel better.
- Let's see a roadmap
    - Use props, don't be scared of window.location.reload(), move logic from JS to PHP, use forms
- Javascript:: package, makes it available in window. Great...but, how'd it get there again?
- Zaengle: Sharing Data in a Laravel/Vue Application
- Instead, let's just return the tweets in the view. Then define a prop on twitter-timelone and add the prop of initial-tweets with json_encode($tweets)
- Lightbulb moment; I could've done it on my own, but felt like I needed someone else needed to "let" me
- Now we use the prop, and we can drop created
- Why named initialTweets? It's immutable. We can change tweets, but doesn't make sense to change initial tweets.
- Now we can kill off loading wheel, etc.
- Now we're simplifying the tweet. We just look at the root and assert that the view has 5 tweets

#### Don't be scared of window.location.relad()

- this.tweets came from the back-end in postTweets, but we're pushing to it in the front-end. Can be drift. Or...you can do window.location.reload()
- Now we don't need this.tweets!
- If the user uses caching, then the perceived front-end change isn't too bad

#### Move logic from JS to PHP

- Remember, props are immutable. Let's split out tweets into freshTweets and icymiTweets. Now using composition logic in the controller instead of just in Vue
- Now it's in the scope of PHPUnit! That means it executed composition logic! No longer need to mock out the axios call, etc.

#### Use Forms

- When posting tweet, still resolving/rejecting promise
- Now let's do a form element that calls api/tweet. @submit="document.forms.postTweet.submit()". But that fails, no payload.
- Add a hidden input element with a :value of tweet. Can get crazy when submitting objects/arrays.
- Now we can kill our method

#### What about validation?

- Oh...we broke it. How do we get validation errors? Just pass it through as an errors prop and pass in the errors messagebag as an array...and we're good.
- Now it's a much simplified flow of data. UI to Server, Server to props, props to UI

#### Summarize
- Use props
- Don't be scared of window.location.reload()
- Move logic from JS to PHP
- Use forms

@calebporzio
