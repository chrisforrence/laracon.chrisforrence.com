---
extends: _layouts/master
section: body
---

### Bob Martin, The Clean Coder

- Tomorrow will be the longest lunar eclipse of this century (all 18 years)
- Maybe we should talk about the moon
- When I was a kid, my science textbook showed where the moon came from (like maybe an errant asteroid that came by, formed together, the moon bubbled out of the Earth). Very old textbook
- One problem, circular orbit. You can't capture something into a circular orbit; it'd become elliptical. Only way to get ciruclar orbit could be a co-creation event.
- Problem with co-creation is the moon orbit has more to do with the sun than the Earth
- Another problem is tidal lock; they had to have started close
- Last little bit of evidence is that the Moon and Earth are made from almost the same materical (including isotope ratio)
- Moon and Earth formed out of the same material...except Earth has iron and Moon has none
- Way they resolved these issues was with a catasrophe. Mars-esque thing collided with Earth, iron cores melted to center to Earth...
- Oh...the talk is about Architecture
- My son was writing a RoR application. Looked at source code, noticed something interesting
- "Why is it that the first thing I see about the dir structure is that it's a Rails app?"
- Why does the framework yell at me that it's a Rails app?
- 1999 was the pinnacle of the DotCom era
- How many programmers in the world (even including PHP programmers? :p)
- Maybe 1/80? Is that possible? Let's presume 100m programmers in the world.
- How long ago did the first line of code execute? 1946, Turing, base-32
- Linear growth urve? No...
- Let's say it doubles every 5 years. Does that sound right?
- 1999 was a hiccup. Programmers usually have jobs.
- If you think very carefully, every second of every day, you are in contact with some computer system. Your grandmother is in contact with some computer system. In our society, everything has a computer in it.
- How much code is running in your _car_?
- Our society depends on us, programmers. They don't get this. We don't get this. Our society's reliance on computers (and thus, us) is huge.
- Can you imagine the discussion the software engineers had for VW emissions cheating? We could check the GPS and the velocity
- CEO: "It was just some software developers that did it for some awful reason"
- And to be fair _we_ wrote that code.
- 1999, world went upside down. Thought, everything's changed, web has come, dot come everything's different. For, say 15 years, we've coasted along believing that.
- About 2012...huh, maybe nothing really had changed. Slightly different formats, but.
- Does anyone remember the emphasis we had of design and architecture in mid-90s?
- Then the web came along and blew that out of our brains for, say, 25 years.
- So MY SON hands me the source code.

#### The web is an IO device; The web is a delivery mechanism!

- Just a way to get stuff out and get stuff in!
- Heck, we learned in the 60s about device independence. And now the device dominates the architecture.

- What does the architecture
- The shape of the structure screams its intent

#### Architecture is about INTENT

- What I should have seen when I looked at the directory structure was intent, not that it's a Rails app!
- Object-Oriented Software Engineering: **A Use Case Driven Approach**
- Learn Forth over a weekend, you'll get a huge appreciation for PHP!
- Use cases are application-specific.

```
Create Order
Data: <Customer-id>, <Shipment-destination>, <payment-information>, <Customer-contact-info>, <Shipment-mchanism>
Primary Course:
1. Order clerk issues "Create Order" command with above data.
2. System validates all data.
3. System creates order and determines order-id.
4. System delivers order-id to clerk.
Exception Course: Validation Error
1. System delivers error message to clerk
```

- You've got these use cases, you can turn them into objects, interactors.
- Interactors have application-specific business rules.
- Entities have application-independent business rules.
- Interactor also relates to Boundary objects (shown as interfaces)

```
User <---> Delivery Mechanism (e.g. HTML)
Delivery Mechanism -> Boundary into Interactor.
```

- Delivery mechanism creates some request model which gets passed into input boundary, gets passed into interactor, which interacts with entities. Coaleses into result model, which gout out boundary, through delivery mechanism back to user
- The framework is an IO device. Not part of the application. It's just tools to help you do _this_.
- The framework should be hidden away, visible as needed

#### What about MVC?

- Introduced by Trygve Reenskaug
- MVC is not an architecture.
- Model object understands simple business rules (e.g. time on screen). Model understands time, hour hours and minutes work. No idea how it gets displayed.
- Controller object watched for input, capture input and use those to send commands to model to mutate data.
- View object observes the model. Whenever the model changes, let the view know, and view updates
- Nowadays in "web architecture" MVC...no really good separation between view/models/controllers
- Instead, maybe Model-View-Presenter
    - Architectural boundary between Presenter/View and Boundary/Interactor/ResponseModel/Entities
    - Presenter is just a plugin to the business rules
    - The web interface should just be a plugin to the business rules. Plug in a console interface. Unplug that. And it should work. Service-oriented interface. Should work.
    - We want independence from our devices
    - The job of the presenter is to convert between the response model for the view model
- How many use TDD? roughly 10% (apparently Java crowd, 33%, Ruby, 50%)
    - Yeah...remember October 1, 2013? The day the ACA website was supposed to go online. Developers didn't communicate properly to managers that they *really* shouldn't turn the site on
- Some developer that you may know may cause an error and kill 10,000. And then what? Then what? Politicans will point their fingers to CEO, CEO will point to us. "How could you let this happen?"
    - If we don't comunicate properly, we'll become regulated.
- Doctors avoided law regulations by coming up with their own.
- We should identify our own regulations and enforce them ourselves.
- Being able to test in isolation is a good indicator of a decent architecture.

#### What about the database?

- Is the database the great god? Where does this thing go? Stick it in the database.
- Think of a database as non-existing.

- Story: FitNesse, fully integrated stand-alone acceptance testing framework and wiki.
- First deferal, MockWikiPage, WikiPage+toHtml+save. InMemoryPages stored in Hashtable
- Or...you could write Hashtable to filesystem. FileSystemPage
- Three months later, decided we didn't need the database after all. We pushed that decision off the project.

#### A good architecture allows major decisions to be deferred to when you have more information

A good architecture maximizes the number of decisions NOT made

- We want the peripheral data to be written a plugins. Don't want this bit? Unplug it.

- 8thlight.com for development
- cleancoder.com for training
