---
extends: _layouts/master
section: body
---

### Samantha Geitz, Full-Stack Testing Strategies

#### "Jest Do It!"

- @SamanthaGeitz, @TightenCo
- Talking about Vue Testing, live-coding ("I'm kinda a masochist")
- Second Annual Tighten Dev Battle: Vue vs React
- The two important libraries for the day
    - jest
    - vue-test-utils
- Jest is the _test runner_ (similar to Mocha)
- Jest lets you _assert_ things using _matchers_
    - I expect 2 + 2 to equal 4
- Jest lets you _mock_ things
    - I don't really want to call this API, so I'll pretend that I did.
- Jest lets you _snapshot_ things
- Jest plays well with Vue and React, pretty straightforward to set up
- vue-test-utils lets you test Vue-specific code (similar to React's Enczyme)
- VTU lets you MOUNT your Vue components (test the component in isolation)
- Lets you MANIPULATE your Vue components
- Test the rendered HTML output
    - I clicked this button, I should see this
- To set up...npm install jest vue-jest jest-serializer-vue @vueblahblah
- But you can do `composer require tightenco/laravel-preset-jest && php artisan fe-preset vue`
- Caleb Porzio has an awesome blog post about configuring Vue+Jest if you want to do it yourself
- What we're really building...pronounced GIF or JIF poller

#### 👩‍💻 Code will be available on GitHub later

- Three fields: GIF or GIF radio buttons, textfield for why, textfield for name
- Stubbed out Routes file that just loads a home view, which has the Vue goodness
- With Jest, you can create a \*.spec.js file
- You can change the directory structure of your spec files in package.json
- When writing Jest tests...

```
describe('GifGif', () => {
    it('renders', () => {
        let wrapper = mount(GifGif);
    })
});
```

- Starts off with a reference error; mount is not defined.
- `import { mount } from '@vue/test-utils';`
- `import GifGif from './GifGif';`
- Next, in the it function:
- `expect(wrapper.text()).toContain('Sup Laracon!');`
- It fails because it doesn't contain that.
- `expect(wrapper.text()).toContain('How do you pronounce the word gif?');`
- It passes
- `expect(wrapper).toMatchSnapshot();`
- Snapshot (took a while to come in) takes your component and renders it to a string
- When changing one of the GIF to JIF, test fails on toMatchSnapshot()
- That's the basics!

#### Next...

```
import { shallowMount } from '@vue/test-utils';
it('updates the selected pronounciation', () => {
    // Check that the radio button updates our vue
    let wrapper = shallowMount(GifGif); // shallowMount only mounts that component
    wrapper.find('#jif').trigger('click');
    expect(wrapper.vm.picked).toBe('jif');
    wrapper.find('#gif').trigger('click');
    expect(wrapper.vm.picked).toBe('gif');
});
```

- Fails, no data.
- Set `picked` in data, fails, doesn't change.
- HTML project changes to include v-mount on radio buttons

```
it('submits a vote', () => {
    let vote = {
        picked: 'gif',
        reason: 'Because',
        name: 'Samantha'
    };

    // trigger the radio button of pronounciation
    wrapper.find('#gif').trigger('click');
    expect(wrapper.vm.picked).toBe('gif');

    // update the reason input
    wrapper.find('#reason').trigger('click');
    expect(wrapper.vm.picked).toBe('gif');

    // update the name input

    // Click the submit button

    // Check that the vote is added to votes

    // Check that the vote is rendered somewhere
});
```

- Declare a wrapper in a beforeEach() to keep from having to define wrapper in each.
- Changed data to be newVote: {picked, reason, name}

```
it('submits a vote', () => {
    let vote = {
        picked: 'gif',
        reason: 'Because',
        name: 'Samantha'
    };

    expect(wrapper.vm.votes.length).toBe(0);

    // trigger the radio button of pronounciation
    wrapper.find('#gif').trigger('click');
    expect(wrapper.vm.picked).toBe('gif');

    // update the reason input
    let reason = wrapper.find('#reason')
    reason.element.value = 'Because'
    reason.trigger('input')
    expect(wrapper.vm.newVote.reason).toBe('Because');

    // update the name input
    let name = wrapper.find('#name')
    name.element.value = 'Because'
    name.trigger('input')
    expect(wrapper.vm.newVote.name).toBe('Because');

    // Click the submit button
    wrapper.find('#submit').trigger('click')

    // Check that the vote is added to votes
    expect(wrapper.vm.votes.length).toBe(1);

    // Check that the vote is rendered somewhere
});
```

- Add votes: [] to data
- Added a method for "add", added onclick handler on submit button to call "add"
- Expect the text to be rendered somewhere
- In HTML, add v-for
- Now the snapshot fails because the text has changed
- Form submits in the browser, but doesn't clear out. Add new expect
- `expect(wrapper.vm.newVote.reason).not.toBe(vote.reason);`
- In the `add()` method, set newVote to the empty-ish object structure we had previously
- And hey, tests pass! But now, you can successfully when you have no fields
- `it('cannot submit without all fields')`
- Find the submit button, click it, then expect the number of votes to be 0 (fails)
- Add computed property named valid (which checks if fields valid)
- Add `:disabled="!valid"` to submit button
- Add `:class="{'opacity-50 cursor-not-allowed': !valid }"`
- Update snapshot, tests pass!
- So now back in Routes, we'll fetch the votes from the database and pass that to the view.
- New `it('renders with props'`
    - expect wrapper's text to contain vote reason and name
- In the beforeEach wrapper, `shallowMount(GifGif, {propsData: { dataVotes: []}})`
- When testing it renders with props, set the propsData: { dataVotes: [vote]}});, test snapshot, great!

#### Finally, let's try and test from the API

Post to a votes route, pretty basic

- `expect(axios.post).toBeCalledWith('/votes')`
- Fails...we need to mock it and add it on the window.
- `window.axios = { post: jest.fn().mockImplmentation(() => Promise.resolve([]))};`
- Quick interlude on JavaScript Promises (it's async; once something happens, handle it appropriately)
- In the add method,

```
axios.post('/api/votes', this.newVote).then(() => {
    this.votes.push(this.newVote)
    this.newVote = {
        picked: '',
        reason: '',
        name: ''
    }
});
```

- Test doesn't work...why? Vue does nextTick
- `it ('does thing', async () => { ... })`
- `await wrapper.vm.$nextTick();`
- Jest docs are great
